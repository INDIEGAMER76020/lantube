#! /usr/bin/python3
# utf-8
import re

import requests
from bs4 import BeautifulSoup
from bs4.element import Tag

from Control.Channel import Channel, Video, Playlist


def query(query_token: str, session: requests.Session = requests.session()) -> list:
    connection = session.get('https://www.youtube.com/results?search_query={query}'.format(query=query_token))
    videos = []

    if connection.status_code == 200:
        # contains
        responseText = BeautifulSoup(connection.text, features='html.parser')
        print('response', responseText)

        # Container contains all findings from the search
        proposalsContainer = responseText.find('div', attrs={'class': 'ytd-item-section-renderer'})
        assert proposalsContainer is not None

        # collects the found hits for the search
        hits = proposalsContainer.find_all('div', attrs={'class': 'yt-lockup'})
        assert hits is not None

        for hit in hits:
            assert isinstance(hit, Tag)

            image = hit.find('img').attrs.get('src')

            duration = hit.find('span', attrs={'class': 'video-time'})
            if duration is not None:
                duration = duration.text

            boxTitle = hit.find('h3', attrs={'class': 'yt-lockup-title'})
            boxAuthor = hit.find('div', attrs={'class': 'yt-lockup-byline'})

            title = boxTitle.find('a').text
            link = boxTitle.find('a').attrs.get('href')
            token = ''.join(re.findall('(?<=v=|t=)[A-Za-z0-9\\-_]+$', link))
            additionalInfo = boxTitle.find('span', attrs={'class': 'accessible-description'}).text

            if boxAuthor is not None:
                channel = boxAuthor.find('a').text
                channelLink = boxAuthor.find('a').attrs.get('href')
            else:
                channel, channelLink = None, None

            if 'Kanal' in additionalInfo:
                element = Channel(channelName=title, channelLink=link, image=image)
            elif 'Playlist' in additionalInfo:
                element = Playlist(channelName=channel, channelLink=channelLink, title=title, image=image, token=token)
            else:
                element = Video(channelName=channel, channelLink=channelLink, title=title, image=image, token=token,
                                duration=duration)
            videos.append(element)

    return videos


def playlist(query_token: str, session: requests.Session = requests.session()) -> (list, str):
    connection = session.get('https://www.youtube.com/playlist?list={query}'.format(query=query_token))
    videos = []
    playlistInfos = ''

    if connection.status_code == 200:
        # videos = connection.text.split('\n')
        responseText = BeautifulSoup(connection.text, features='html.parser')

        # title search
        titleHeader = responseText.find('div', attrs={'class': 'pl-header-content'})
        title = titleHeader.find('h1', attrs={'class': 'pl-header-title'}).text.strip()
        channel = titleHeader.find('a', attrs={'class': 'yt-uix-sessionlink'}).text.strip()
        titleInfos = titleHeader.find_all('li')
        videoCount, callCount = '', ''
        if len(titleInfos) > 1:
            videoCount = titleInfos[1].text
        if len(titleInfos) > 2:
            callCount = titleInfos[2].text
        playlistInfos = '{title} - {channel}%N%{videos}%N%{calls}'.format(
            title=title, channel=channel, videos=videoCount, calls=callCount
        )

        # videos
        tableElements = responseText.find_all('tr', attrs={'class': 'pl-video'})
        for element in tableElements:
            assert isinstance(element, Tag)

            title = element.find('a', attrs={'class': 'pl-video-title-link'})
            if title is not None:
                title = title.text.strip()

            channelContainer = element.find('div', attrs={'class': 'pl-video-owner'})
            if channelContainer is not None:
                channelName = channelContainer.find('a').text
                channelLink = channelContainer.find('a').attrs.get('href')
            else:
                channelName, channelLink = '', ''

            image = element.find('img').attrs.get('src')
            token = element.attrs.get('data-video-id')
            duration = element.find('div', attrs={'class': 'timestamp'}).find('span').text

            videos.append(Video(channelName=channelName, channelLink=channelLink, title=title, image=image, token=token,
                                duration=duration))

    return videos, playlistInfos
