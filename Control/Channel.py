#! /usr/share/python3
# utf-8
import requests


class Channel(object):
    def __init__(self, channelName: str, channelLink: str, image: str, session: requests.Session = requests.session()):
        """

        :param channelName: name of the channel
        :param channelLink: link to the channel
        :param image: link to the attached image
        """
        self.__name = channelName
        self.__link = channelLink
        self.__image = image
        self.__session = session

    def getChannelName(self) -> str: return self.__name

    def getChannelLink(self) -> str: return self.__link

    def getImageLink(self) -> str: return self.__image

    def getSession(self) -> requests.Session: return self.__session


class Playlist(Channel):
    def __init__(self, channelName: str, channelLink: str, image: str, title: str, token: str):
        """

        :param channelName: author of the video (channel name)
        :param channelLink: link to the channel
        :param image: link to the thumbnail image
        :param title: title of the video
        """
        super().__init__(channelName=channelName, channelLink=channelLink, image=image)

        self.__title = title
        self.__token = token

    def getToken(self) -> str:
        return self.__token

    def getTitle(self) -> str:
        return self.__title


class Video(Playlist):
    def __init__(self, channelName: str, channelLink: str, image: str, title: str, duration: str, token: str):
        """

        :param channelName: author of the video (channel name)
        :param channelLink: link to the channel
        :param image: link to the thumbnail image
        :param title: title of the video
        :param duration: duration of the video
        :param token: youtube token of the video
        """
        super().__init__(channelName=channelName, channelLink=channelLink, image=image, title=title, token=token)

        self.__duration = duration

    def getDuration(self) -> str:
        return self.__duration
