#! /usr/bin/python3
# utf-8
import html
import os

import cherrypy
import requests

from Control import Search
from Control.Channel import Channel, Video, Playlist

# Name Value Domain Path Expires Size HttpOnly Secure SameSite SameParty Priority
# CONSENT	YES+cb.20210509-17-p0.de+F+497	.google.de	/	2038-01-10T07:59:59.611Z	37		✓	None		Medium
# NID	215=UPBSOQYulBzQyJjZrpOP6e_6IFjKkD7pXH2uKHC5HYAP7do5ajSVR6Gw2xuHkyqTQwfC8OxOhZUPRQBmVrWjBIhD0lLVkDmiNMiQYv4AYYCMGSpbZZA7lA1IbHmNniqC0rZof72uxRo3r2f_BmyZqP3i42EUqPRZNuSIEjFYzIg	.google.com	/	2021-11-16T19:24:20.520Z	178	✓	✓	None		Medium
# ANID	OPT_OUT	.google.de	/	2030-11-09T23:58:59.611Z	11	✓	✓	None		Medium
# PREF	f5=30000&tz=Europe.Berlin&f6=400	.youtube.com	/	2023-05-17T19:26:27.000Z	36		✓			Medium
# ANID	OPT_OUT	.google.com	/	2030-11-09T23:58:59.520Z	11	✓	✓	None		Medium
# YSC	bXykejPXhGo	.youtube.com	/	Session	14	✓	✓	None		Medium
# VISITOR_INFO1_LIVE	KGpo8ntcOAY	.youtube.com	/	2021-11-13T19:24:20.428Z	29	✓	✓	None		Medium
# GPS	1	.youtube.com	/	2021-05-17T19:56:24.119Z	4	✓	✓			Medium
# NID	215=bjMSg9GpUjUEkF8j8lIvDsyxApaFlnraNsoq1TbiVb0VbeBNqIqp5w7R6Qy6ChRRiyhNwWXSacMyhDPw_RnxGFpQ4C9qK7RVWT73Bb1QBBPw3aFwTmHm_3txhu8-7QlA5zVbDSUwlhvwNYq5GW_lSxyZ9_CD5Xu8KAM-PUSrBpk	.google.de	/	2021-11-16T19:24:20.611Z	178	✓	✓	None		Medium
# CONSENT	YES+cb.20210509-17-p0.de+F+708	.google.com	/	2038-01-10T07:59:59.520Z	37		✓	None		Medium
# CONSENT	YES+cb.20210509-17-p0.de+F+541	.youtube.com	/	2038-01-10T07:59:59.428Z	37		✓	None	Medium
#
# YSC	1Wc-qlGv16M	.youtube.com	/	Session	14	✓	✓	None		Medium
# wide	0	.youtube.com	/	2022-06-03T23:59:36.000Z	5					Medium
# PREF	f5=30000&tz=Europe.Berlin&f6=400	.youtube.com	/	2023-06-03T18:39:03.000Z	36		✓			Medium
# GPS	1	.youtube.com	/	2021-06-03T19:09:03.009Z	4	✓	✓			Medium
# VISITOR_INFO1_LIVE	ppVjfcJ6XYM	.youtube.com	/	2021-11-30T18:07:08.318Z	29	✓	✓	None		Medium
# CONSENT	YES+cb.20210530-19-p0.de+F+758	.youtube.com	/	2038-01-10T07:59:59.318Z	37		✓	None		Medium

# https://stackoverflow.com/questions/7164679/how-to-send-cookies-in-a-post-request-with-the-python-requests-library

globalSession = requests.session()

pathHTML = "/Frontend/HTML"

cookiesGoogleCom = dict(Name='CONSENT', Value='YES+cb.20210509-17-p0.de+F+497')
cookie = dict(ysc="1Wc-qlGv16M", wide="0", pref="f5=30000&tz=Europe.Berlin&f6=400", gps="1",
              visitor_info1_live="ppVjfcJ6XYM", consent="YES+cb.20210530-19-p0.de+F+758")


def prepareHtmlChannels(elements: list) -> str:
    if len(elements) > 0 and not isinstance(elements[0], Channel):
        return ''
    elif len(elements) < 0:
        return ''

    output = ''
    prefixChannel = '<a href="https://www.youtube.com{channelLink}">{channelName}</a>'
    for element in elements:
        if isinstance(element, Channel) and not (isinstance(element, Playlist) or isinstance(element, Video)):
            output += prefixChannel.format(channelLink=element.getChannelLink(),
                                           channelName=html.escape(element.getChannelName()))
            output += '<br>'

    return output


def prepareHtmlPlaylists(elements: list) -> str:
    if len(elements) > 0 and not isinstance(elements[0], Channel):
        return ''
    elif len(elements) < 0:
        return ''

    output = ''
    prefixChannel = '<a href="https://www.youtube.com{channelLink}">{channelName}</a>'
    prefixPlaylist = ' - <a href="/list?token={token}">{title}</a>'
    for element in elements:
        if isinstance(element, Playlist) and not isinstance(element, Video):
            output += prefixChannel.format(channelLink=element.getChannelLink(),
                                           channelName=html.escape(element.getChannelName()))
            output += prefixPlaylist.format(token=element.getToken(), title=html.escape(element.getTitle()))
            output += '<br>'

    return output


def prepareHtmlVideos(elements: list) -> str:
    if len(elements) > 0 and not isinstance(elements[0], Channel):
        return ''
    elif len(elements) < 0:
        return ''

    output = ''
    prefixChannel = '<a href="https://www.youtube.com{channelLink}">{channelName}</a>'
    prefixVideo = ' - <a href="/video?token={token}">{title}</a>'
    for element in elements:
        if isinstance(element, Video):
            output += prefixChannel.format(channelLink=element.getChannelLink(),
                                           channelName=html.escape(element.getChannelName()))
            output += prefixVideo.format(token=element.getToken(), title=html.escape(element.getTitle()))
            output += ' - {}'.format(element.getDuration())
            output += '<br>'

    return output


class Website(object):

    def __init__(self):
        self.videos = []

        # globalSession.cookies.

    @cherrypy.expose
    def index(self):
        path = '{}/index.HTML'.format(pathHTML)
        if os.path.isfile(path):
            return open(path)
        else:
            return 'FILE NOT FOUND: {}'.format(path)

    @cherrypy.expose
    def query(self, token):
        path = '{}/query.HTML'.format(pathHTML)
        videos = Search.query(token, session=globalSession)
        self.videos += videos

        content = ''
        if os.path.isfile(path):
            with open(path, 'r') as htmlWebsite:
                content = htmlWebsite.read()
                content = content.replace('%QUERY%', html.escape(token, quote=True))
                content = content.replace('%CHANNEL%', prepareHtmlChannels(videos))
                content = content.replace('%PLAYLIST%', prepareHtmlPlaylists(videos))
                content = content.replace('%VIDEOLIST%', prepareHtmlVideos(videos))
        else:
            content = 'FILE NOT FOUND: {}'.format(path)

        return content

    @cherrypy.expose
    def list(self, token):
        path = '{}/list.HTML'.format(pathHTML)
        videos, playlist = Search.playlist(token, session=globalSession)
        self.videos += videos

        content = ''
        if os.path.isfile(path):
            with open(path, 'r') as htmlWebsite:
                content = htmlWebsite.read()
                content = content.replace('%PLAYLIST%', html.escape(playlist).replace('%N%', '<br>'))
                content = content.replace('%VIDEOLIST%', prepareHtmlVideos(videos))
                print(len(videos), videos)
        else:
            content = 'FILE NOT FOUND: {}'.format(path)

        return content

    @cherrypy.expose
    def video(self, token):
        path = 'HTML/video.HTML'
        title = ''
        for element in self.videos:  # searching for a known video from the last request and gets the title
            if isinstance(element, Video) and element.getToken() == token:
                title = element.getTitle()

        if os.path.isfile(path):
            with open(path, 'r') as htmlWebsite:
                return htmlWebsite.read() \
                    .replace('%VIDEOTITLE%', html.escape(title)) \
                    .replace('%TOKEN%', html.escape(token, quote=True))
        else:
            return 'FILE NOT FOUND: {}'.format(path)


if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True
        }
    }
    cherrypy.quickstart(Website(), '/', conf)
